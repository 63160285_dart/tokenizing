import 'dart:io';

List tokenizing(String input) {
  List result = [];
  for (var i = 0; i < input.length; i++) {
    if (input[i] == ' ') {
      result.add(input[i]);
      result.remove(input[i]);
    } else {
      result.add(input[i]);
    }
  }
  result = tokenizingNegative(result);
  return result;
}
//if input has a negative number
List tokenizingNegative(List tokens) {
  for (var i = 0; i < tokens.length - 1; i++) {
    for (var j = i + 1; j < tokens.length; j++) {
      if (tokens[i] == '-' && tokens[j]== '(') {
        break;
      } else if (tokens[i]=='-') {
        var temp = tokens[j];
        tokens[i] = '-$temp';
        tokens.remove(tokens[j]);
      }
    }
  }
  return tokens;
}

//check level of operators
int precedence(String operators) {
  if (operators == "(" || operators == ")") {
    return 4;
  }
  if (operators == "^") {
    return 3;
  }
  if (operators == "*" || operators == "/") {
    return 2;
  }
  if (operators == "%") {
    return 3;
  }
  if (operators == "+" || operators == "-") {
    return 1;
  }
  return 0;
}

bool isInteger(String string) {
  if (string == null) {
    return false;
  }
  return double.tryParse(string) != null;
}
//for change infix to postfix
List ChangetoPostfix(List infix) {
  List operators = [];
  List postfix = [];
  infix.forEach((i) {
    if (isInteger(i)) {
      postfix.add(i);
    } else if (isInteger(i) == false && i != '(' && i != ')') {
      while (operators.isNotEmpty &&
          operators.last != '(' &&
          precedence(i) < precedence(operators.last)) {
        postfix.add(operators.removeLast());
      }
      operators.add(i);
    } else if (i == '(') {
      operators.add(i);
    } else if (i == ')') {
      while (operators.last != '(') {
        postfix.add(operators.removeLast());
      }
      operators.remove('(');
    }
  });
  while (operators.isNotEmpty) {
    postfix.add(operators.removeLast());
  }
  return postfix;
}

double evaluatePostfix(List postfix) {
  List values = [];
  double left, right;

  postfix.forEach((i) {
    if (isInteger(i)) {
      double temp = double.parse(i);
      values.add(temp);
    } else {
      right = values.removeLast();
      left = values.removeLast();
      var result = calculator(left, right, i);
      values.add(result);
    }
  });
  return values.first;
}
//Create when line 96 call calcolator function for Apply the operator to left and right
//and Append the result to the end of values in line 97
double calculator(double left, double right, String operators) {
  double result = 0;
  switch (operators) {
    case "+":
      {
        result = left + right;
      }
      break;
    case "-":
      {
        result = left - right;
      }
      break;
    case "*":
      {
        result = left * right;
      }
      break;
    case "/":
      {
        result = left / right;
      }
      break;
    case "%":
      {
        result = left % right;
      }
      break;
    case "^":
      {
        result = exponent(left, right);
      }
      break;
    default:
      {}
      break;
  }

  return result;
}

double exponent(double left, double right) {
  double expo = left;
  double result = 1;
  for (int i = 0; i < right; i++) {
    result = result * expo;
  }
  return result;
}

void main(List<String> args) {
  print('Input your mathematical expression:');
  String input = stdin.readLineSync()!;
  var infix = tokenizing(input);
  print('Your infix: $infix');
  var postfix = ChangetoPostfix(infix);
  print('Your postfix: $postfix');
  var result = evaluatePostfix(postfix);
  print('Your result of postfix is : $result');
}
